package main

import (
	"fmt"
	"os"
)

type WebserverConfig struct {
	Port      int
	Interface string
	Endpoint  string
}

type DuolingoConfig struct {
	Token    string
	Username string
	Password string
	Endpoint string
}

func getDuolingoConfig() DuolingoConfig {
	duolingoUsername := os.Getenv("USERNAME")
	duolingoPassword := os.Getenv("PASSWORD")

	duolingoEndpoint, ok := os.LookupEnv("ENDPOINT")
	if !ok {
		duolingoEndpoint = "https://www.duolingo.com"
	}

	fmt.Printf("Running Goduo with username %s and endpoint %s\n", duolingoUsername, duolingoEndpoint)
	return DuolingoConfig{Username: duolingoUsername, Password: duolingoPassword, Endpoint: duolingoEndpoint}
}

func getWebserverConfig() WebserverConfig {
	return WebserverConfig{Port: 8080, Interface: "0.0.0.0", Endpoint: "/metrics"}
}
