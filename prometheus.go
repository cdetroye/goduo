package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

func truncateToDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func sanitizeValue(value string) string {
	var sanitized string

	re := regexp.MustCompile("[[:^ascii:]]")
	sanitized = re.ReplaceAllString(value, "")
	sanitized = strings.ReplaceAll(sanitized, " ", "_")
	sanitized = strings.ReplaceAll(sanitized, "(", "")
	sanitized = strings.ReplaceAll(sanitized, ")", "")
	return sanitized
}

func dataToPrometheus(data AccountData) (string, error) {
	var response string

	// Current streak.
	response += fmt.Sprintf("streak %d\n", data.Streak)
	// Previous streak.
	response += fmt.Sprintf("previous_streak %d\n", data.LastStreak.Length)
	// Language points.
	var total = 0
	for _, language := range data.Languages {
		var sanitizedLanguage = sanitizeValue(language.Language)
		var line = fmt.Sprintf("language_%s %d\n", sanitizedLanguage, language.Points)
		response += line
		total += language.Points
	}

	// Get the current time in local timezone.
	// The timestamps are in the timezone of the user.
	userTimezone := data.Timezone
	loc, _ := time.LoadLocation(userTimezone)
	nowTz := time.Now().In(loc)
	dayTz := truncateToDay(nowTz)

	lessons := 0
	for _, languageData := range data.LanguageData {
		for _, event := range languageData.Calendar {

			eventDateTz := event.Date.In(loc)
			eventDayTz := truncateToDay(eventDateTz)

			if eventDayTz == dayTz {
				lessons += 1
			}
		}
	}
	response += fmt.Sprintf("lessons_today %d\n", lessons)

	response += fmt.Sprintf("total %d\n", total)
	return response, nil
}
