package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// GetDuolingoToken Attempts to get a JWT token from the API.
// Returns an error if failed.
func GetDuolingoToken(config DuolingoConfig) (string, error) {
	postBody, _ := json.Marshal(map[string]string{
		"login":    config.Username,
		"password": config.Password,
	})

	responseBody := bytes.NewBuffer(postBody)

	resp, err := http.Post(fmt.Sprintf("%s/login", config.Endpoint), "application/json", responseBody)

	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	// Check login succeeded.
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return "", errors.New("duolingo authentication failed")
	}

	// Read the body into memory.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	jsonString := string(body)

	// Check if it was an error or regular response.
	var reply map[string]string
	err = json.Unmarshal(body, &reply)

	if failMessage, ok := reply["failure"]; ok {
		return "", errors.New(failMessage)
	}

	// Assuming it's a positive reply, check for fields.
	var response AuthResponse
	err = json.Unmarshal([]byte(jsonString), &response)
	if err != nil {
		return "", err
	}

	if response.Response == "" {
		return "", errors.New("invalid response. missing response")
	}

	if response.UserId == "" {
		return "", errors.New("invalid response. missing user id")
	}

	if response.Username == "" {
		return "", errors.New("invalid response. missing username")
	}

	// Get the cookies from the request, and pick out the jwt_token field.
	cookies := resp.Cookies()
	var token string
	for _, cookie := range cookies {
		if cookie.Name == "jwt_token" {
			token = cookie.Value
		}
	}
	return token, nil
}

func (ls *EventList) UnmarshalJSON(data []byte) error {
	evts := []Event{}
	if err := json.Unmarshal(data, &evts); err != nil {
		return err
	}

	// Dump all the events that don't have a type.
	for _, p := range evts {
		//if p.Type != "" {
		*ls = append(*ls, p)
		//}
	}

	return nil
}

func (evt *Event) UnmarshalJSON(data []byte) error {
	var f interface{}
	err := json.Unmarshal(data, &f)
	if err != nil {
		return err
	}
	m := f.(map[string]interface{})

	if m["event_type"] != nil {
		evt.Type = m["event_type"].(string)
	}
	millis := int64(m["datetime"].(float64))
	evt.Date = time.Unix(0, millis*int64(time.Millisecond))
	return nil
}

// getAccountData  Gets the total XP points per language on Duolingo.
func getAccountData(username string, token string, config DuolingoConfig) (AccountData, error) {
	// Create a request.
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/users/%s", config.Endpoint, username), nil)
	if err != nil {
		return AccountData{}, err
	}
	// Set the header using the provided token.
	req.Header.Set("Authorization", "Bearer "+token)

	// Execute the request.
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return AccountData{}, err
	}
	defer resp.Body.Close()

	// Check request succeeded.
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return AccountData{}, errors.New(fmt.Sprintf("Server response %d, request for user '%s' data failed.", resp.StatusCode, username))
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return AccountData{}, err
	}

	//os.WriteFile("output.json", []byte(string(body)), 0644)

	// Unmarshall the response into a struct and return it.
	var data AccountData
	err = json.Unmarshal([]byte(string(body)), &data)
	if err != nil {
		return AccountData{}, err
	}

	if len(data.Languages) == 0 {
		return AccountData{}, errors.New("could not parse languages from response")
	}
	return data, nil
}
