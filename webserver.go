package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
)

func getUserData(config DuolingoConfig, username string) (string, error) {
	// Attempt to get a token.
	var token, err = GetDuolingoToken(config)
	if err != nil {
		log.Printf("Error getting Duolingo token: %s", err)
		return "", errors.New("Error getting token from Duolingo API.")
	}

	// Attempt to get the user data using the above token.
	languageData, err := getAccountData(username, token, config)
	if err != nil {
		return "", err
	}

	str, err := dataToPrometheus(languageData)
	return str, nil
}

func metricsHandler(config DuolingoConfig) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		log.Printf("%s\n", req.RequestURI)

		var username = req.URL.Query().Get("target")

		// Check if there is a valid username.
		// Return 500 if the username is invalid.
		match, _ := regexp.MatchString("[[:alnum:]]+", username)
		if !match {
			log.Printf("500: '%s' is an invalid username.\n", username)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "invalid username")
			return
		}

		// Get the user data and reply.
		data, err := getUserData(config, username)
		if err != nil {
			log.Printf("Error getting the points for %s: %s", username, err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "unknown error")
			return
		}

		// Set 200 and reply.
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, data)
	}
}
