# GoDuo 

Goduo is a Prometheus exporter for Duolingo users. 
It exports the following metrics:

 - Total XP
 - Longest streak
 - Current streak
 - XP per language

# Config

Store your Duolingo login in an `.env` file. For example:

```bash
USERNAME=duolingousername
PASSWORD=myduolingopassword
```
# Prometheus Config

Use the config below. Put all the usernames you wish to track in the targets list.
Fill in the right endpoint at the bottom.

```yaml
  - job_name: 'goduo'
    scrape_interval: 15m
    metrics_path: /metrics
    static_configs:
     - targets:
       - myself
       - thatotherpersonidontknowatall
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: goduoendpoint:8090
```

# Grafana

The Grafana dashboard can be found in `grafana/`.
