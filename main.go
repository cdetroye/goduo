//package main
//
//import (
//	"fmt"
//	"log"
//	"net/http"
//	"time"
//)

//func main() {
//	i, err := strconv.ParseInt("1668079917000", 10, 64)
//	if err != nil {
//		panic(err)
//	}
//	//tm := time.Unix(i, 0)
//	tm := time.Unix(0, i*int64(time.Millisecond))
//	fmt.Println(tm)
//}

package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

type Language struct {
	Points   int    `json:"points"`
	Language string `json:"language_string"`
}

type Streak struct {
	Length int `json:"length"`
}

type Event struct {
	Type string    `json:"event_type"`
	Date time.Time `json:"datetime"`
}

type LanguageData struct {
	Streak   int       `json:"stream"`
	Calendar EventList `json:"calendar"`
}

type EventList []Event

type AccountData struct {
	Languages    []Language              `json:"languages"`
	LastStreak   Streak                  `json:"last_streak"`
	Streak       int                     `json:"site_streak"`
	LanguageData map[string]LanguageData `json:"language_data"`
	Timezone     string                  `json:"timezone"`
}

type AuthResponse struct {
	Response string
	Username string
	UserId   string `json:"user_id"`
}

func main() {
	duoConfig := getDuolingoConfig()
	webConfig := getWebserverConfig()

	serverAddress := fmt.Sprintf("%s:%d", webConfig.Interface, webConfig.Port)

	log.Printf("Starting webserver http://%s%s\n", serverAddress, webConfig.Endpoint)

	http.HandleFunc(webConfig.Endpoint, metricsHandler(duoConfig))
	err := http.ListenAndServe(serverAddress, nil)
	if err != nil {
		log.Fatal("Error starting webserver. Reason %s", err)
	}
}
